import React from 'react';
import Login from "./components/Login";
import {Route, Switch} from 'react-router-dom';
import SignUp from "./components/Signup";
import './App.css';
import Entries from "./components/Entries";
import Users from "./components/Users";
import UpdateEntries from "./components/UpdateEntries";
import UpdateUser from "./components/UpdateUser";

const App = () => {

    return (
        <div className="App">
            <Switch>
                <Route exact={true} path="/">
                    <Entries />
                </Route>
                <Route exact={true} path="/users">
                    <Users />
                </Route>
                <Route exact={true} path="/login">
                    <Login/>
                </Route>
                <Route exact={true} path="/sign-up">
                    <SignUp/>
                </Route>
                <Route exact={true} path="/updateEntry">
                    <UpdateEntries/>
                </Route>
                <Route exact={true} path="/updateUser">
                    <UpdateUser/>
                </Route>
            </Switch>
        </div>
    );
}

export default App;
