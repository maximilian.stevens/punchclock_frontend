import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";

const UpdateEntries = () => {

    const [currentUser, setCurrentUser] = useState(JSON.parse(localStorage.getItem("user")));
    const [checkInDate, setCheckInDate] = useState("")
    const [checkInTime, setCheckInTime] = useState("")
    const [checkOutDate, setCheckOutDate] = useState("")
    const [checkOutTime, setCheckOutTime] = useState("")
    const [entry, setEntry] = useState(null)
    const [id, setId] = useState("")
    const history = useHistory();
    const [categories, setCategories] = useState([])
    const [category, setCategory] = useState("Work")


    useEffect(() => {
        if (localStorage.getItem('JWT') === null || localStorage.getItem("user") === null) {
            history.push("/login")
        } else {
            const url = new URL(window.location.href);
            const id = url.searchParams.get("id");
            setId(id);
            getEntry(id)
            getCategories();
        }
    }, [history])

    useEffect(() => {
        if (entry !== null) {
            setCheckInDate(entry.checkIn.substring(0, 10))
            setCheckInTime(entry.checkIn.substring(11, 19))
            setCheckOutDate(entry.checkOut.substring(0, 10))
            setCheckOutTime(entry.checkOut.substring(11, 19))
        }
    }, [entry])

    const updateEntry = (e) => {
        e.preventDefault();
        const entry = {};
        const selectedCategory = categories.filter(item => item.name === category)
        entry['checkIn'] = dateAndTimeToDate(checkInDate, checkInTime);
        entry['checkOut'] = dateAndTimeToDate(checkOutDate, checkOutTime);
        entry['category'] = selectedCategory[0];
        entry['user'] = currentUser;
        postEntry(entry, id)
    }

    const postEntry = (entry, id) => {
        fetch(`http://localhost:8081/entries/${id}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("JWT")
            },
            body: JSON.stringify(entry)
        }).then((resp) => {
            if (resp.status === 200) {
                history.push("/")
            }
        })
    }

    const getEntry = (id) => {
        fetch(`http://localhost:8081/entries/${id}`, {
            headers: {
                'Authorization': localStorage.getItem('JWT')
            }
        }).then((resp) => resp.json())
            .then(data => setEntry(data));
    }

    const getCategories = () => {
        fetch("http://localhost:8081/categories", {
            headers: {
                'Authorization': localStorage.getItem('JWT')
            },
        }).then((resp) => {
            if (resp.status === 200) {
                resp.json().then(data => {
                    setCategories(data)
                })
            }
        })
    }

    const dateAndTimeToDate = (dateString, timeString) => {
        return dateString + "T" + timeString + "Z";
    };

    return (
        <>
            <form id="createEntryForm">
                <label htmlFor="checkIn">
                    Update
                </label>
                <input
                    type="date"
                    value={checkInDate}
                    onChange={(e) => setCheckInDate(e.target.value)}
                    name="checkInDate"
                    id="checkIn"
                    required
                />
                <input
                    type="time"
                    value={checkInTime}
                    onChange={(e) => setCheckInTime(e.target.value)}
                    name="checkInTime"
                    required
                />
                <label htmlFor="checkOut">Check out</label>
                <input
                    type="date"
                    value={checkOutDate}
                    onChange={(e) => setCheckOutDate(e.target.value)}
                    name="checkOutDate"
                    id="checkOut"
                    required
                />
                <input
                    type="time"
                    value={checkOutTime}
                    onChange={(e) => setCheckOutTime(e.target.value)}
                    name="checkOutTime"
                    required
                />
                <select
                    name="category"
                    id="category"
                    style={{marginTop: '10px'}}
                    onChange={(e) => setCategory(e.target.value)}
                    value={category}
                >
                    {categories.map(category => {
                        return (
                            <option key={category.id} value={category.name}>{category.name}</option>
                        )
                    })}
                </select>
                <input
                    type="submit"
                    onClick={(e) => history.push("/")}
                    value="Back"
                />
                <input
                    type="submit"
                    onClick={(e) => updateEntry(e)}
                    value="Save Entry"
                />
            </form>
        </>
    )
}

export default UpdateEntries;