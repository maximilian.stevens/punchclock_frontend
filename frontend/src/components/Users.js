import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";

const Users = () => {

    const [users, setUsers] = useState([]);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [roles, setRoles] = useState([])
    const [role, setRole] = useState("Admin")
    const history = useHistory();

    useEffect(() => {
        if (localStorage.getItem('JWT') === null) {
            history.push("/login")
        }
        getUsers();
        getRoles();
    }, [history])


    const createUser = (e) => {
        e.preventDefault();
        const selectedRole = roles.filter(item => item.name === role)
        const user = {
            username,
            password,
            role: selectedRole[0]
        };
        postUserCreate(user)
    };

    const postUserCreate = (user) => {
        fetch("http://localhost:8081/users/sign-up", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('JWT')
            },
            body: JSON.stringify(user)
        }).then((resp) => {
            if (resp.status === 201) {
                getUsers();
            }
        })
    }

    const deleteUser = (id) => {
        fetch(`http://localhost:8081/users/${id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': localStorage.getItem('JWT')
            }
        }).then((resp) => {
            if (resp.status === 200) {
                getUsers();
            } else {
                alert("something went wrong...")
            }
        })
    }

    const getRoles = () => {
        fetch('http://localhost:8081/roles', {
            headers: {
                'Authorization': localStorage.getItem('JWT')
            }
        }).then((resp) => resp.json())
            .then(roles => {
                setRoles(roles)
                setRole(roles[0].name)
            })
    }

    const getUsers = () => {
        fetch("http://localhost:8081/users", {
            headers: {
                'Authorization': localStorage.getItem('JWT')
            }
        }).then(result => result.json())
            .then(data => setUsers(data));
    };

    return (
        <>
            <form id="createEntryForm" style={{width: '450px'}}>
                <div className="form-group left">
                    <label htmlFor="username"> Username </label>
                    <input
                        onChange={(e) => setUsername(e.target.value)}
                        value={username}
                        type="text"
                        className="form-control"
                        placeholder="Enter Username"
                        name="username"
                        id="username"
                        required
                    />
                </div>
                <div className="form-group left">
                    <label htmlFor="password"> Password </label>
                    <input
                        onChange={(e) => setPassword(e.target.value)}
                        value={password}
                        type="text"
                        className="form-control"
                        placeholder="Enter password"
                        name="password"
                        id="password"
                        required
                    />
                </div>
                <select
                    name="category"
                    id="category"
                    style={{marginTop: '10px'}}
                    onChange={(e) => setRole(e.target.value)}
                    value={role}
                >
                    {roles.map(role => {
                        return (
                            <option key={role.id} value={role.name}>{role.name}</option>
                        )
                    })}
                </select>
                <input
                    type="submit"
                    onClick={(e) => createUser(e)}
                    value="Create User"
                />
            </form>
            <table>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Role</th>
                </tr>
                </thead>
                <tbody id="entryDisplay">
                {
                    users.map((user) => {
                        return (
                            <tr key={user.id}>
                                <td>{user.id}</td>
                                <td>{user.username}</td>
                                <td>{user.password}</td>
                                <td>{user.role.name}</td>
                                <td>
                                    <button onClick={() => history.push(`/updateUser?id=${user.id}`)}> Update
                                    </button>
                                </td>
                                <td>
                                    <button onClick={() => deleteUser(user.id)}> Delete</button>
                                </td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
            <button onClick={() => history.push("/")}> User UI</button>

        </>
    )
}

export default Users;