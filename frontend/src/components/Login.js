import React, {useState} from "react";
import {Link, useHistory} from "react-router-dom";

const Login = () => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const history = useHistory();

    const login = () => {
        let user = {
            username,
            password,
        };
        postLogin(user)
    };

    const postLogin = (user) => {
        fetch("http://localhost:8081/login", {
            method: 'POST',
            headers: {
                "Content-Type": 'application/json'
            },
            body: JSON.stringify(user)
        })
            .then(resp => {
                if (resp.status === 200) {
                    localStorage.setItem("JWT", resp.headers.get('Authorization'))
                    getUser(user);
                }
            });
    }

    const getUser = (user) => {
        fetch("http://localhost:8081/users/findUser", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("JWT")
            },
            body: JSON.stringify(user)
        }).then((resp) => {
            if (resp.status === 200) {
                resp.json().then(data => {
                    localStorage.setItem("user", JSON.stringify(data))
                    history.push("/")
                });
            } else {
                alert("something went wrong")
            }
        })
    }

    return (
        <div className="auth-wrapper">
            <div className="auth-inner">
                <form>
                    <h3>Login </h3>
                    <div className="form-group">
                        <label>Email address</label>
                        <input
                            onChange={(e) => setUsername(e.target.value)}
                            value={username}
                            type="email"
                            id="test3"
                            className="form-control"
                            placeholder="Enter email"
                        />
                    </div>

                    <div className="form-group">
                        <label>Password</label>
                        <input
                            onChange={(e) => setPassword(e.target.value)}
                            value={password}
                            type="password"
                            id="pw"
                            className="form-control"
                            placeholder="Enter password"
                        />
                    </div>

                    <button
                        type="button"
                        id="submitBtn"
                        onClick={() => login()}
                        className="btn btn-primary btn-block"
                    >
                        Submit
                    </button>
                    <p className="forgot-password text-right">
                        New to us? <Link to="/sign-up"> Sign up!</Link>
                    </p>
                </form>
            </div>
        </div>
    );
};

export default Login;
