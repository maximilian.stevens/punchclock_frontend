import React, {useState} from "react";
import {Link, useHistory} from "react-router-dom";

const SignUp = () => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [usernameUsed, setUsernameUsed] = useState(false);
    const [responseError, setResponseError] = useState(false);
    const history = useHistory();

    const signup = () => {
        let user = {
            username,
            password,
        }
        findUserAndSignUp(user);
    };

    const findUserAndSignUp = (user) => {
        fetch("http://localhost:8081/users/findUser", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("JWT")
            },
            body: JSON.stringify(user)
        }).then((resp) => {
            if (resp.status === 404) {
                signUp(user);
            } else {
                setUsernameUsed(true)
            }
        });
    }

    const signUp = (user) => {
        fetch("http://localhost:8081/users/sign-up", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("JWT")
            },
            body: JSON.stringify(user)
        }).then((resp) => {
            resp.json().then(data => {
                localStorage.setItem("user", JSON.stringify(data))
            })
            if (resp.status === 201) {
                fetch("http://localhost:8081/login", {
                    method: 'POST',
                    headers: {
                        "Content-Type": 'application/json'
                    },
                    body: JSON.stringify(user)
                })
                    .then(resp => {
                        if (resp.status === 200) {
                            localStorage.setItem("JWT", resp.headers.get('Authorization'))
                            history.push("/")
                        } else {
                            setResponseError(true)
                        }
                    });
            }
        });
    }

    return (
        <div className="auth-wrapper">
            <div className="auth-inner">
                <form>
                    <h3>Create Account </h3>
                    <div className="form-group left">
                        <label>Email address</label>
                        <input
                            onChange={(e) => setUsername(e.target.value)}
                            value={username}
                            type="email"
                            className="form-control"
                            placeholder="Enter email"
                        />
                    </div>
                    <div className="form-group left">
                        <label>Password</label>
                        <input
                            onChange={(e) => setPassword(e.target.value)}
                            value={password}
                            type="password"
                            className="form-control"
                            placeholder="Enter password"
                        />
                    </div>
                    <button
                        type="button"
                        id="submitBtn"
                        onClick={signup}
                        className="btn btn-primary btn-block"
                    >
                        Create Account
                    </button>
                    {usernameUsed ? <div style={{color: 'red'}}> Username already in use </div> : null}
                    {responseError ? <div style={{color: 'red'}}> Something went wrong... </div> : null}
                    <p className="forgot-password text-right">
                        Already registered? <Link to="/login"> Sign in! </Link>
                    </p>
                </form>
            </div>
        </div>
    );
};

export default SignUp;
