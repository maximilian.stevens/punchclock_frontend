import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";

const UpdateUser = () => {

    const [currentUser, setCurrentUser] = useState("");
    const [id, setId] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [roles, setRoles] = useState([]);
    const [role, setRole] = useState("Admin");
    const [entries, setEntries] = useState("");
    const history = useHistory();

    useEffect(() => {
        if (localStorage.getItem('JWT') === null) {
            history.push("/login")
        } else {
            const url = new URL(window.location.href);
            const id = url.searchParams.get("id");
            setId(id);
            getUser(id);
            getRoles();
        }
    }, [history])

    useEffect(() => {
        if (currentUser !== "") {
            setUsername(currentUser.username)
            setPassword(currentUser.password)
            setEntries(currentUser.entries)
            setRole(currentUser.role)
        }
    }, [currentUser])


    const updateUser = (e) => {
        e.preventDefault();
        const selectedRole = roles.filter(item => item.name === role)
        let user = {
            username,
            password,
            entries,
            role: selectedRole[0]
        };
        postUserUpdate(user)
    };

    const postUserUpdate = (user) => {
        fetch(`http://localhost:8081/users/${id}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("JWT")
            },
            body: JSON.stringify(user)
        }).then((resp) => {
            if (resp.status === 200) {
                history.push("/users")
            }
        });
    }

    const getUser = (id) => {
        fetch(`http://localhost:8081/users/${id}`, {
            headers: {
                'Authorization': localStorage.getItem('JWT')
            }
        }).then((resp) => resp.json())
            .then(user => setCurrentUser(user))
    }

    const getRoles = () => {
        fetch('http://localhost:8081/roles', {
            headers: {
                'Authorization': localStorage.getItem('JWT')
            }
        }).then((resp) => resp.json())
            .then(roles => {
                setRoles(roles)
                setRole(roles[0].name)
            })
    }

    return (
        <>
            <form id="createEntryForm" style={{width: '450px'}}>
                <div className="form-group left">
                    <label htmlFor="username"> Username </label>
                    <input
                        onChange={(e) => setUsername(e.target.value)}
                        value={username}
                        type="text"
                        className="form-control"
                        placeholder="Enter Username"
                        name="username"
                        id="username"
                        required
                    />
                </div>
                <div className="form-group left">
                    <label htmlFor="password"> Password </label>
                    <input
                        onChange={(e) => setPassword(e.target.value)}
                        value={password}
                        type="text"
                        className="form-control"
                        placeholder="Enter password"
                        name="password"
                        id="password"
                        required
                    />
                </div>
                <select
                    name="category"
                    id="category"
                    style={{marginTop: '10px'}}
                    onChange={(e) => setRole(e.target.value)}
                    value={role}
                >
                    {roles.map(role => {
                        return (
                            <option key={role.id} value={role.name}>{role.name}</option>
                        )
                    })}
                </select>
                <input
                    type="submit"
                    onClick={(e) => history.push("/users")}
                    value="Back"
                />
                <input
                    type="submit"
                    onClick={(e) => updateUser(e)}
                    value="Save Entry"
                />
            </form>
        </>
    )
}

export default UpdateUser;