import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";

const Entries = () => {

    const [currentUser, setCurrentUser] = useState(JSON.parse(localStorage.getItem("user")));
    const [entries, setEntries] = useState([]);
    const [checkInDate, setCheckInDate] = useState(new Date().toISOString().substr(0, 10))
    const [checkInTime, setCheckInTime] = useState(new Date().toISOString().substring(11, 19))
    const [checkOutDate, setCheckOutDate] = useState(checkInDate)
    const [checkOutTime, setCheckOutTime] = useState(checkInTime)
    const history = useHistory();
    const [categories, setCategories] = useState([])
    const [category, setCategory] = useState("Work")

    useEffect(() => {
        if (localStorage.getItem('JWT') === null || localStorage.getItem("user") === null) {
            history.push("/login")
        }
        getUser(currentUser)
        getCategories();
    }, [])

    useEffect(() => {
        localStorage.setItem("user", JSON.stringify(currentUser))
    }, [currentUser])

    const createEntry = (e) => {
        e.preventDefault();
        const entry = {};
        const selectedCategory = categories.filter(item => item.name === category)
        entry['checkIn'] = dateAndTimeToDate(checkInDate, checkInTime);
        entry['checkOut'] = dateAndTimeToDate(checkOutDate, checkOutTime);
        entry['category'] = selectedCategory[0];
        entry['user'] = currentUser;
        postEntryCreate(entry);
    };

    const postEntryCreate = (entry) => {
        fetch("http://localhost:8081/entries", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('JWT')
            },
            body: JSON.stringify(entry)
        }).then((resp) => {
            if (resp.status === 201) {
                resp.json().then(data => {
                    let array = [...entries]
                    array.push(data);
                    setEntries([...array])
                })
            }
        })
    }

    const getUser = (user) => {
        fetch("http://localhost:8081/users/findUser", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("JWT")
            },
            body: JSON.stringify(user)
        }).then((resp) => resp.json())
            .then(data => {
                setCurrentUser(data)
                setEntries(data.entries)
            });
    };

    const deleteEntry = (id) => {
        fetch(`http://localhost:8081/entries/${id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': localStorage.getItem('JWT')
            }
        }).then((resp) => {
            if (resp.status === 200) {
                getUser(currentUser);
            } else {
                alert("something went wrong...")
            }
        })
    }

    const getCategories = () => {
        fetch("http://localhost:8081/categories", {
            headers: {
                'Authorization': localStorage.getItem('JWT')
            },
        }).then((resp) => {
            if (resp.status === 200) {
                resp.json().then(data => {
                    setCategories(data)
                })
            }
        })
    }

    const dateAndTimeToDate = (dateString, timeString) => {
        return dateString + "T" + timeString + "Z";
    };

    return (
        <>
            <form id="createEntryForm">
                <label htmlFor="checkIn">
                    Check in
                </label>
                <input
                    type="date"
                    value={checkInDate}
                    onChange={(e) => setCheckInDate(e.target.value)}
                    name="checkInDate"
                    id="checkIn"
                    required
                />
                <input
                    type="time"
                    value={checkInTime}
                    onChange={(e) => setCheckInTime(e.target.value)}
                    name="checkInTime"
                    required
                />
                <label htmlFor="checkOut">Check out</label>
                <input
                    type="date"
                    value={checkOutDate}
                    onChange={(e) => setCheckOutDate(e.target.value)}
                    name="checkOutDate"
                    id="checkOut"
                    required
                />
                <input
                    type="time"
                    value={checkOutTime}
                    onChange={(e) => setCheckOutTime(e.target.value)}
                    name="checkOutTime"
                    required
                />
                <select
                    name="category"
                    id="category"
                    style={{marginTop: '10px'}}
                    onChange={(e) => setCategory(e.target.value)}
                >
                    {categories.map(category => {
                        return (
                            <option key={category.id} value={category.name}>{category.name}</option>
                        )
                    })}
                </select>
                <input
                    type="submit"
                    onClick={(e) => createEntry(e)}
                    value="Create Entry"
                />
            </form>
            <table>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Check in</th>
                    <th>Check out</th>
                    <th>Category</th>
                </tr>
                </thead>
                <tbody>
                {
                    entries.map((entry) => {
                        return (
                            <tr key={entry.id}>
                                <td>{entry.id}</td>
                                <td>{entry.checkIn}</td>
                                <td>{entry.checkOut}</td>
                                <td>{entry.category.name}</td>
                                <td>
                                    <button onClick={() => history.push(`/updateEntry?id=${entry.id}`)}> Update</button>
                                </td>
                                <td>
                                    <button onClick={() => deleteEntry(entry.id)}> Delete</button>
                                </td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
            <button
                onClick={() => history.push("/users")}
                disabled={currentUser.role.name !== 'Admin'}>
                Admin UI
            </button>
            <button onClick={() => {
                window.localStorage.clear();
                history.push("/login")
            }}>
                Logout
            </button>
        </>
    )
}

export default Entries;